

function createCard(name, description, pictureUrl, dates, location) {
  return `
    <div class="card shadow-lg p-3 mb-5 bg-body-tertiary rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <div class="card-subtitle mb-2 text-muted">${location}</div>
        <p class="card-text">${description}</p>
        <div class="card-footer">
          ${dates}
        </div>
      </div>
    </div>
  `;
}

function alertMessage(message) {
  return `
    <div class="alert alert-warning" role="alert">
          ${message}
    </div>
  `;
}


function renderPlaceholder() {
  return `
    <div class="card" aria-hidden="true">
    <div class="card-body">
      <h5 class="card-title placeholder-glow">
        <span class="placeholder col-6"></span>
      </h5>
      <p class="card-text placeholder-glow">
        <span class="placeholder col-7"></span>
        <span class="placeholder col-4"></span>
        <span class="placeholder col-4"></span>
        <span class="placeholder col-6"></span>
        <span class="placeholder col-8"></span>
      </p>
      <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
    </div>
  </div>
  `;
}


window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      const errorPlaceholder = document.getElementById("error-placeholder")
      errorPlaceholder.innerHTML = alertMessage("bad API response.")
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();
      let index = 0;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        const placeholder = renderPlaceholder()
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const column = document.querySelector(`#col-${index % 3}`);
          column.innerHTML += placeholder;

          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const location = details.conference.location.name;

          var startDate = new Date(details.conference.starts);
          startDate = startDate.toDateString();
          var endDate = new Date(details.conference.ends);
          endDate = endDate.toDateString();
          const dates = `${startDate} - ${endDate}`;

          const html = createCard(title, description, pictureUrl, dates, location);

          column.innerHTML += html;
          index += 1;

        }
      }

    }
  } catch (e) {
    console.log(e);
  }

});
