
document.addEventListener("DOMContentLoaded", async () => {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);
    const selectTag = document.getElementById("conference");


    if (response.ok) {
        const data = await response.json();
        for (let conference of data.conferences) {
            const html = createOption(conference.name, conference.id);
            selectTag.innerHTML += html;
        }
    }


    const formTag = document.getElementById("create-presentation-form");
    formTag.addEventListener("submit", async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const selectTag = document.getElementById("conference");
        const json = JSON.stringify(Object.fromEntries(formData));
        const option = selectTag.selectedOptions;
        const conferenceId = option[0].value;
        const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                "Content-Type": "application/json"
            }
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
            console.log(newPresentation);
        }
    });
});

function createOption(name, value) {
    return `
        <option value="${value}">${name}</option>
    `;
}
